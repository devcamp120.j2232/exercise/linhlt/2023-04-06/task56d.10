package com.devcamp.rainbowrequestinputapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rainbowrequestinputapi.service.RainbowService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class RainbowController {
    @Autowired
    RainbowService rainbowService;

    @GetMapping("/rainbow-request-query")
    public ArrayList<String> searchRainbows(@RequestParam (value="q", defaultValue = "") String keyword){
        //RainbowService rainbowService = new RainbowService();
        return rainbowService.searchRainbows(keyword);
    }
    @GetMapping("/rainbow-request-param/{index}")
    public String getRainbow(@PathVariable int index){
        return rainbowService.getRainbow(index);
    }
}
