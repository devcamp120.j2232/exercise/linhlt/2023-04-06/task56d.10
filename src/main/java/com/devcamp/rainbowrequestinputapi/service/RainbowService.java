package com.devcamp.rainbowrequestinputapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;
@Service
public class RainbowService {
    private String[] listRainbows = {"red","orange", "yellow", "green", "blue", "indigo", "violet"};
    public ArrayList<String> searchRainbows(String keyword){
        ArrayList<String> rainbows = new ArrayList<>();
        for (String rainbow: listRainbows){
            if (rainbow.contains(keyword)){
                rainbows.add(rainbow);
            }
        }
        return rainbows;
    }
    public String getRainbow(int index){
        String rainbow = "";
        if (index >= 0 && index <= 6){
            rainbow = listRainbows[index];
        }
        return rainbow;
    }
}
